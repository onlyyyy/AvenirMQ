//测试服务端
const net = require('net');
const port = 13000;
const ip = '127.0.0.1';
const amq = require('avenirmq');
const { toLog } = require('./common/common');

async function main() {
    this.server = new net.createServer();
    await amq.init({
        ip, port:52013,
    })
    let sign = await amq.login({ name:'test', password: '123456'});
    console.log("sign = ",sign);
    setInterval(async () => {
        let data = {a:Math.random().toString().slice(1)};
        console.log("开始自动发送 data = ",data);
        await amq.send(data,sign.data);
    }, 2*1000);
    this.server.on('connection', async (client) => {
        
        client.on('data', async (msg) => { //接收client发来的信息
            console.log("收到服务器发来消息", JSON.parse(msg));
            client.write(JSON.stringify({
                code: 0,
                message: 'success',
            }))
        });

        client.on('error', function (e) { //监听客户端异常
            console.log('client error' + e);
            client.end();
        });

        client.on('close', function () {
            console.log(`客户端下线了`);
        });
    });
    this.server.listen(port, ip, function () {
        console.log(`client1运行在：${ip}:${port}`);
    });

}


main();